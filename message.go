package contactagon

import (
	"errors"
	"fmt"
	"github.com/microcosm-cc/bluemonday"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
	"net/http"
	"os"
	"regexp"
	"strings"
	"time"
)

// Message represents a message sent to the API, e.g. from an HTTP POST.
type Message struct {
	Name			string `json:"name" firestore:"name"`
	Email			string `json:"email" firestore:"email"`
	Phone			string `json:"phone" firestore:"phone"`
	ReplyBy			string `json:"reply_by" firestore:"reply_by"`
	Subject			string `json:"subject" firestore:"subject"`
	Message			string `json:"message" firestore:"message"`
	Captcha			string `json:"captcha" firestore:"-"`
	Time 			int64  `json:"time,omitempty" firestore:"time"`
	IP				string `json:"ip,omitempty" firestore:"ip"`
}


// CheckLimit returns true if the sender's IP has been found fewer than 5 times in
// the last 30 seconds in message storage. If the IP has been found more than 5
// times in the last 30 seconds, returns false. As storage may be asynchronous
// and/or inconsistent, this is a rough mechanism, but it's a useful extra
// layer of abuse prevention. You should still deploy behind a rate-limted
// API gateway!
func (message *Message) CheckLimit() bool {
	delta := time.Now().Unix() - 30
	messages := storageService.client.GetMessagesSince(delta, message.IP)
	if len(messages) < 5 {
		return true
	}
	logger.Warn().Msgf("%s exceeded rate limit", message.IP)
	return false
}

func (message *Message) Format() string {
	header := fmt.Sprintf("Contactagon Message from %s <%s> (%s):", message.Name, message.Email, message.Subject)
	footer := strings.Join([]string{message.Name, message.Email, message.Phone}, "\n")
	footer = footer + "\n" + "contact-by: " + message.ReplyBy
	return strings.Join([]string{header, message.Message, footer}, "\n\n")
}

func (message *Message) GenerateEmail() *mail.SGMailV3 {
	from := mail.NewEmail(message.Name, os.Getenv("CONTACTAGON_FROM_ADDRESS"))
	subject := fmt.Sprintf("[via contactagon] %s", message.Subject)
	to := mail.NewEmail(os.Getenv("CONTACTAGON_TO_NAME"), os.Getenv("CONTACTAGON_TO_ADDRESS"))
	email := mail.NewSingleEmailPlainText(from, subject, to, message.Format())
	email.ReplyTo = mail.NewEmail(message.Name, message.Email)
	return email
}

func (message *Message) HandlePost(writer http.ResponseWriter, request *http.Request) {
	remoteip := getRemoteIP(request)
	if err := message.Validate(); err != nil {
		logger.Error().Msgf("invalid message from %s (%s): %s", request.RemoteAddr, message.Email, err.Error())
		http.Error(writer, "http/400 message failed validation", 400)
		return
	}
	if err := message.VerifyCaptcha(remoteip); err != nil {
		http.Error(writer, "http/400 recaptcha verification failed", 400)
		return
	}
	if !message.CheckLimit() {
		http.Error(writer, "http/429 rate limit exceeded", 429)
		return
	}
	if strings.ToLower(os.Getenv("CONTACTAGON_SANITIZE")) == "true" {
		message.Sanitize()
	}
	if err := message.Save(getRemoteIP(request)); err != nil {
		http.Error(writer, "http/500 error saving message", 500)
		return
	}
	if err := message.SendMail(); err != nil {
		http.Error(writer, "http/500 error sending message", 500)
		return
	}
	writer.WriteHeader(http.StatusAccepted)
	_, _ = writer.Write([]byte("accepted"))
}

func (message *Message) Sanitize() {
	sanitizer := bluemonday.UGCPolicy()
	message.Message = sanitizer.Sanitize(message.Message)
}

func (message *Message) Save(remoteAddr string) error {
	if err := storageService.client.Save(remoteAddr, *message); err != nil {
		return err
	}
	return nil
}

func (message *Message) SendMail() error {
	email := message.GenerateEmail()
	return SendMail(email)
}

func (message *Message) Validate() error {
	stringMatch := regexp.MustCompile(`^[\p{L} .\-',]{2,254}$`)
	if !stringMatch.MatchString(message.Name) {
		return errors.New("invalid name")
	}
	if !stringMatch.MatchString(message.Subject) {
		return errors.New("invalid subject")
	}
	emailMatch := regexp.MustCompile(`^[\w\.+\-]{1,64}@(?:[a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-]{2,}$`)
	if !emailMatch.MatchString(message.Email) || len(message.Email) > 320 {
		return errors.New("invalid email address")
	}
	if len(message.Phone) > 0 {
		if match, _ := regexp.MatchString(`^[0-9]{7,35}$`, message.Phone); match != true {
			return errors.New("invalid phone number")
		}
	}
	if match, _ := regexp.MatchString(`^[\w_-]{1,1000}$`, message.Captcha); match != true {
		return errors.New("invalid captcha")
	}
	if len(message.ReplyBy) > 0 {
		if message.ReplyBy != "email" && message.ReplyBy != "phone" && message.ReplyBy != "text" {
			return errors.New("invalid reply_by value")
		}
	}
	if len(message.Message) < 1 || len(message.Message) > 10240 {
		return errors.New("invalid message body")
	}
	return nil
}

func (message *Message) VerifyCaptcha(remoteip string) error {
	verification, err := CaptchaVerify(message.Captcha, remoteip)
	if err != nil {
		return err
	}
	if !verification.Success {
		msg := fmt.Sprintf("recaptcha verification from %s for %s failed", remoteip, message.Email)
		logger.Error().Msg(msg)
		return errors.New(msg)
	}
	return nil
}
