package contactagon

import (
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"strings"
	"testing"
)

type SuiteMessage struct {
	suite.Suite
	Message Message
}

func (suite *SuiteMessage) SetupTest() {
	suite.Message = Message{
		Name:    "John Doe",
		Email:   "john+nospam@doe.com",
		Phone:   "12345678901",
		ReplyBy: "email",
		Subject: "hello my friend",
		Message: "I have a proposal for you!",
		Captcha: "asdf1234",
		Time:    1633275714,
		IP:      "129.79.1.1",
	}
}

func (suite *SuiteMessage) TestMessageValidateValid() {
	assert.Nil(suite.T(), suite.Message.Validate())
}

func (suite *SuiteMessage) TestMessageValidateNameSupportsUnicode() {
	message := suite.Message
	message.Name = "Bjørn Cañada Schleßinger D'Andréa"
	assert.Nil(suite.T(), message.Validate())
}

func (suite *SuiteMessage) TestMessageValidateNameTooShort() {
	message := suite.Message
	message.Name = "j"
	assert.Error(suite.T(), message.Validate(), "invalid name")
}

func (suite *SuiteMessage) TestMessageValidateNameTooLong() {
	message := suite.Message
	first := randomString(128)
	last := randomString(150)
	message.Name = fmt.Sprintf("%s %s", first, last)
	valid := message.Validate()
	assert.Error(suite.T(), valid, "invalid name")
}

func (suite *SuiteMessage) TestMessageValidateEmailInvalidDomain() {
	message := suite.Message
	message.Email = "nobu+foob@barbell"
	assert.Error(suite.T(), message.Validate(), "invalid email address")
}

func (suite *SuiteMessage) TestMessageValidateEmailLocalPartTooLong() {
	message := suite.Message
	message.Email = randomString(65) + "@gmail.com"
	assert.Error(suite.T(), message.Validate(), "invalid email address")
}

func (suite *SuiteMessage) TestMessageValidateEmailTooLong() {
	message := suite.Message
	message.Email = randomString(60) + "@" + randomString(260) + ".net"
	assert.Error(suite.T(), message.Validate(), "invalid email address")
}

func (suite *SuiteMessage) TestMessageValidatePhoneAlpha() {
	message := suite.Message
	message.Phone = "bravo-12345"
	assert.Error(suite.T(), message.Validate(), "invalid phone number")
}

func (suite *SuiteMessage) TestMessageValidatePhonePunc() {
	message := suite.Message
	message.Phone = "+1 (510) 555-1223 x 555"
	assert.Error(suite.T(), message.Validate(), "invalid phone number")
}

func (suite *SuiteMessage) TestMessageValidatePhoneTooLong() {
	message := suite.Message
	message.Phone = "649236643243945349326282385353586382"
	assert.Error(suite.T(), message.Validate(), "invalid phone number")
}

func (suite *SuiteMessage) TestMessageValidatePhoneTooShort() {
	message := suite.Message
	message.Phone = "12345"
	assert.Error(suite.T(), message.Validate(), "invalid phone number")
}

func (suite *SuiteMessage) TestMessageValidateReplyByEmail() {
	message := suite.Message
	message.ReplyBy = "email"
	assert.Nil(suite.T(), message.Validate())
}

func (suite *SuiteMessage) TestMessageValidateReplyByPhone() {
	message := suite.Message
	message.ReplyBy = "phone"
	assert.Nil(suite.T(), message.Validate())
}

func (suite *SuiteMessage) TestMessageValidateReplyByTelegraph() {
	message := suite.Message
	message.ReplyBy = "telegraph"
	assert.Error(suite.T(), message.Validate(), "invalid reply_by value")
}

func (suite *SuiteMessage) TestMessageValidateReplyByText() {
	message := suite.Message
	message.ReplyBy = "text"
	assert.Nil(suite.T(), message.Validate())
}

func (suite *SuiteMessage) TestMessageValidateReplyByEmpty() {
	message := suite.Message
	message.ReplyBy = ""
	assert.Nil(suite.T(), message.Validate())
}

func (suite *SuiteMessage) TestMessageValidateSubjectSupportsUnicode() {
	message := suite.Message
	message.Subject = "Bjørn Cañada Schleßinger D'Andréa"
	assert.Nil(suite.T(), message.Validate())
}

func (suite *SuiteMessage) TestMessageValidateSubjectTooLong() {
	message := suite.Message
	message.Subject = randomString(255)
	assert.Error(suite.T(), message.Validate(), "invalid subject")
}

func (suite *SuiteMessage) TestMessageValidateSubjectTooShort() {
	message := suite.Message
	message.Subject = "s"
	assert.Error(suite.T(), message.Validate(), "invalid subject")
}

func (suite *SuiteMessage) TestMessageValidateBodyRequired() {
	message := suite.Message
	message.Message = ""
	assert.Error(suite.T(), message.Validate(), "invalid Message body")
}

func (suite *SuiteMessage) TestMessageValidateBodyTooLong() {
	message := suite.Message
	message.Message = randomString(20481)
	assert.Error(suite.T(), message.Validate(), "invalid Message body")
}

func (suite *SuiteMessage) TestMessageValidateCaptchaRequired() {
	message := suite.Message
	message.Captcha = ""
	assert.Error(suite.T(), message.Validate(), "invalid captcha")
}

func (suite *SuiteMessage) TestMessageValidateCaptchaPunctuation() {
	message := suite.Message
	message.Captcha = "Hello, world (look at me)!"
	assert.Error(suite.T(), message.Validate(), "invalid captcha")
}

func (suite *SuiteMessage) TestMessageValidateCaptchaTooLong() {
	message := suite.Message
	message.Captcha = randomString(1001)
	assert.Error(suite.T(), message.Validate(), "invalid captcha")
}

func (suite *SuiteMessage) TestMessageParseJson() {
	var message Message
	jsonStr := `{
				"name": "John Doe",
				"email": "john+nospam@doe.com",
				"phone": "12345678901",
				"reply_by": "email",
				"subject": "hello my friend",
				"captcha": "asdf1234",
				"message": "I have a proposal for you!",
				"time": 1633275714,
				"ip": "129.79.1.1"
			}`
	jsonReader := strings.NewReader(jsonStr)
	err := json.NewDecoder(jsonReader).Decode(&message)
	fmt.Println(message)
	assert.NoError(suite.T(), err, "error deserializing json into Message")
	assert.EqualValues(suite.T(), suite.Message, message)
}

func TestSuiteMessage(t *testing.T) {
	suite.Run(t, new(SuiteMessage))
}
