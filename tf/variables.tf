variable "project" {
  type = string
  description = "project id (required)"
}

variable "from_address" {
  type = string
  description = "must be sendgrid verified"
}

variable "to_address" {
  type = string
}

variable "to_name" {
  type = string
}

variable "cors_origins" {
  type = string
  description = "comma-separated list of allowed domains"
}

variable "recaptcha_url" {
  type = string
  description = "recaptcha verification URL"
}

variable "firestore_collection" {
  type = string
}

variable "bucket_location" {
  type = string
}