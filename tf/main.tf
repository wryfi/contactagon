provider "google" {
  project = var.project
  region  = "us-central1"
}

terraform {
  backend "gcs" {}
}

locals {
  timestamp     = formatdate("YYYYMMDDhhmmss", timestamp())
  root_dir      = abspath("../")
}


resource "google_service_account" "contactagon-fn-sa" {
  account_id   = "contactagonfn"
  display_name = "contactagon function"
  description  = "service account for contactagon function"
}

resource "google_service_account" "contactagon-api-sa" {
  account_id   = "contactagonapi"
  display_name = "contactagon api"
  description  = "service account for contactagon api"
}

resource "google_service_account_iam_binding" "api-sa-user-ch" {
  service_account_id = google_service_account.contactagon-api-sa.name
  role               = "roles/iam.serviceAccountUser"
  members            = ["user:chris.haumesser@gmail.com"]
}

resource "google_app_engine_application" "app" {
  project       = var.project
  location_id   = "us-central"
  database_type = "CLOUD_FIRESTORE"
}

resource "google_project_iam_binding" "fn-datastore-user" {
  project = var.project
  role    = "roles/datastore.user"
  members = [
    format("serviceAccount:%s", google_service_account.contactagon-fn-sa.email)
  ]
}

resource "google_secret_manager_secret" "CONTACTAGON_RECAPTCHA_SECRET_KEY" {
  secret_id = "CONTACTAGON_RECAPTCHA_SECRET_KEY"
  replication {
    automatic = true
  }
  lifecycle {
    prevent_destroy = true
  }
}

resource "google_secret_manager_secret" "CONTACTAGON_SENDGRID_API_KEY" {
  secret_id = "CONTACTAGON_SENDGRID_API_KEY"
  replication {
    automatic = true
  }
  lifecycle {
    prevent_destroy = true
  }
}

resource "google_secret_manager_secret_iam_binding" "contactagon-fn-recaptcha" {
  project   = var.project
  secret_id = google_secret_manager_secret.CONTACTAGON_RECAPTCHA_SECRET_KEY.secret_id
  role      = "roles/secretmanager.secretAccessor"
  members = [
    format("serviceAccount:%s", google_service_account.contactagon-fn-sa.email)
  ]
}

resource "google_secret_manager_secret_iam_binding" "contactagon-fn-sendgrid" {
  project   = var.project
  secret_id = google_secret_manager_secret.CONTACTAGON_SENDGRID_API_KEY.secret_id
  role      = "roles/secretmanager.secretAccessor"
  members = [
    format("serviceAccount:%s", google_service_account.contactagon-fn-sa.email)
  ]
}

data "archive_file" "source" {
  type        = "zip"
  source_dir  = local.root_dir
  output_path = "/tmp/contactagon-${local.timestamp}.zip"
  excludes = [".idea", ".git", ".gitignore", ".gcloudignore", "Makefile", "tf", "*_test.go"]
}

resource "google_storage_bucket" "contactagon-fn-bucket" {
  location = var.bucket_location
  name = "${var.project}-contactagon"
}

resource "google_storage_bucket_object" "contactagon-fn-zip" {
  name   = "contactagon-${local.timestamp}.zip"
  bucket = google_storage_bucket.contactagon-fn-bucket.name
  source = data.archive_file.source.output_path
}

resource "google_cloudfunctions_function" "contactagon-message-post" {
  name                  = "contactagon-message-post"
  runtime               = "go113"
  available_memory_mb   = 256
  source_archive_bucket = google_storage_bucket.contactagon-fn-bucket.name
  source_archive_object = google_storage_bucket_object.contactagon-fn-zip.name
  trigger_http          = true
  entry_point           = "MessagePost"
  service_account_email = google_service_account.contactagon-fn-sa.email
  environment_variables = {
    CONTACTAGON_FIRESTORE_COLLECTION = var.firestore_collection
    CONTACTAGON_FROM_ADDRESS = var.from_address
    CONTACTAGON_TO_ADDRESS = var.to_address
    CONTACTAGON_TO_NAME = var.to_name
    CONTACTAGON_CORS_ORIGINS = var.cors_origins
    CONTACTAGON_RECAPTCHA_VERIFY_URL = var.recaptcha_url
    GCP_PROJECT = var.project
  }
}

resource "google_cloudfunctions_function_iam_binding" "contactagon-fn-api-invoker" {
  cloud_function = google_cloudfunctions_function.contactagon-message-post.name
  role = "roles/cloudfunctions.invoker"
  members = ["serviceAccount:${google_service_account.contactagon-api-sa.email}"]
}

resource "google_api_gateway_api" "contactagon-api" {
  provider = google-beta
  api_id = "contactagon-api"
}

resource "google_api_gateway_api_config" "contactagon-api-cfg" {
  provider = google-beta
  project = var.project
  api = google_api_gateway_api.contactagon-api.api_id
  api_config_id = "contactagon-api-cfg"

  openapi_documents {
    document {
      path = "api.yml"
      contents = filebase64("${local.root_dir}/tf/api-${var.project}.yml")
    }
  }
  lifecycle {
    create_before_destroy = true
  }
  gateway_config {
    backend_config {
      google_service_account = google_service_account.contactagon-api-sa.email
    }
  }
}

resource "google_api_gateway_gateway" "contactagon-gateway" {
  provider = google-beta
  project = var.project
  api_config = google_api_gateway_api_config.contactagon-api-cfg.id
  gateway_id = "contactagon-gateway"
  region = "us-central1"
}

resource "null_resource" deleteLocalZip {
  triggers = {
    once = timestamp()
  }

  depends_on = [
    google_cloudfunctions_function.contactagon-message-post,
  ]

  provisioner "local-exec" {
    command = "rm -f /tmp/contactagon-${local.timestamp}.zip"
  }
}

output "function_url" {
  value = google_cloudfunctions_function.contactagon-message-post.https_trigger_url
}

output "api_url" {
  value = "https://${google_api_gateway_gateway.contactagon-gateway.default_hostname}/messages"
}