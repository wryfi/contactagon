module gitlab.com/wryfi/contactagon

go 1.13

require (
	cloud.google.com/go v0.95.0 // indirect
	cloud.google.com/go/firestore v1.5.0
	cloud.google.com/go/functions v0.2.0 // indirect
	cloud.google.com/go/secretmanager v0.1.0
	cloud.google.com/go/storage v1.16.0 // indirect
	github.com/GoogleCloudPlatform/functions-framework-go v1.2.0
	github.com/golang/groupcache v0.0.0-20210331224755-41bb18bfe9da // indirect
	github.com/googleapis/gax-go/v2 v2.1.1 // indirect
	github.com/microcosm-cc/bluemonday v1.0.15
	github.com/rs/zerolog v1.23.0
	github.com/sendgrid/rest v2.6.4+incompatible // indirect
	github.com/sendgrid/sendgrid-go v3.10.0+incompatible
	github.com/stretchr/testify v1.7.0
	golang.org/x/net v0.0.0-20210917221730-978cfadd31cf // indirect
	golang.org/x/sys v0.0.0-20210921065528-437939a70204 // indirect
	golang.org/x/text v0.3.7 // indirect
	google.golang.org/api v0.57.0
	google.golang.org/genproto v0.0.0-20210921142501-181ce0d877f6
)
