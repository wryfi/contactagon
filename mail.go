package contactagon

import (
	"errors"
	"fmt"
	"github.com/rs/zerolog/log"
	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
)

var mailService *MailService

func init() {
	mailService = NewMailService(new(SendGridClient))
}

type MailClient interface {
	SendMail(v3 *mail.SGMailV3) error
}

type MailService struct {
	client MailClient
}

func NewMailService(client MailClient) *MailService {
	return &MailService{client: client}
}

type SendGridClient struct {}

func (grid *SendGridClient) SendMail(message *mail.SGMailV3) error {
	key, err := grid.GetApiKey()
	if err != nil {
		return err
	}
	client := sendgrid.NewSendClient(key)
	response, err := client.Send(message)
	if err != nil {
		logger.Error().Msgf("sendgrid failure while sending Message: %s", err)
		return err
	} else {
		if response.StatusCode != 202 {
			msg := fmt.Sprintf("http/%d from sendgrid: %s", response.StatusCode, response.Body)
			logger.Error().Msg(msg)
			return errors.New(msg)
		}
		logger.Info().Msgf("message from %s sent via sendgrid", message.ReplyTo.Address)
		return nil
	}
}

func (grid *SendGridClient) GetApiKey() (string, error) {
	sendgridKey, err := GetEnvOrSecret("CONTACTAGON_SENDGRID_API_KEY")
	if err != nil {
		log.Error().Msgf("failed to get sendgrid api key: %s", err)
		return "", err
	}
	return sendgridKey, nil
}

func SendMail(message *mail.SGMailV3) error {
	if err := mailService.client.SendMail(message); err != nil {
		log.Error().Msgf("sending message failed: %s", err)
		return err
	}
	return nil
}
