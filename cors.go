package contactagon

import (
	"net/http"
	"os"
	"strings"
)

func manageCors(writer http.ResponseWriter, request *http.Request) {
	allowed := strings.Split(os.Getenv("CONTACTAGON_CORS_ORIGINS"), ",")
	origin := request.Header.Get("origin")
	if contains(allowed, origin) {
		writer.Header().Set("Access-Control-Allow-Origin", origin)
	}
	writer.Header().Set("Access-Control-Allow-Methods", "HEAD, OPTIONS, POST")
	writer.Header().Set(
		"Access-Control-Allow-Headers",
		"Accept, Accept-Encoding, Content-Length, Content-Type, Date, Server, Transfer-Encoding",
	)
}
