package contactagon

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"testing"
)

type MockStorageProvider struct {
	mock.Mock
}

func (smock *MockStorageProvider) GetMessagesSince(time int64, ip string) []Message {
	return []Message{}
}

func (smock *MockStorageProvider) Save(remoteAddr string, message Message) error {
	return nil
}

type SuiteStorage struct {
	suite.Suite
	originalService *StorageService
}

func (suite *SuiteStorage) SetupSuite() {
	suite.originalService = storageService
	mockStorageService := StorageService{new(MockStorageProvider)}
	storageService = &mockStorageService
}

func (suite *SuiteStorage) TearDownSuite() {
	storageService = suite.originalService
}

func (suite *SuiteStorage) TestSave() {
	err := storageService.client.Save("0.0.0.0", Message{})
	assert.NoError(suite.T(), err)
}

func (suite *SuiteStorage) TestGetMessagesSince() {
	messages := storageService.client.GetMessagesSince(5, "nobody@test.local")
	assert.Equal(suite.T(), []Message{}, messages)
}

func TestSuiteStorage(t *testing.T) {
	suite.Run(t, new(SuiteStorage))
}
