package contactagon

import (
	"github.com/rs/zerolog"
	"os"
	"strings"
)

var logger zerolog.Logger

func init() {
	debug := os.Getenv("CONTACTAGON_DEBUG")
	if strings.ToLower(debug) == "true" {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	} else {
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	}
	if strings.ToLower(os.Getenv("CONTACTAGON_PRETTY_LOGS")) == "true" {
		output := zerolog.ConsoleWriter{Out: os.Stderr}
		logger = zerolog.New(output).With().Timestamp().Logger()
	} else {
		zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
		logger = zerolog.New(os.Stderr).With().Timestamp().Logger()
	}
}
