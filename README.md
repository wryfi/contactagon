# contactagon

[contactagon](#) is a suite of Go functions and GCP tools providing a backend for contact forms.

## Features

`MessagePost` is deployed as a Google Cloud Function. It accepts a message via JSON, which it validates, stores in Firestore, and sends to the configured user via Sendgrid.

The provided `api.yml` is used to map `MessagePost` (and future functions) to a GCP API Gateway, which provides rate-limiting and other services in front of the functions.

Security/Abuse protections:

* Firestore data is used as a crude rate-limiting mechanism
  * as an eventually consistent system, there is often a short delay between writes and subsequent reads
  * this reduces the precision of rate-limiting accordingly
* Rate-limiting is also imposed by API Gateway using the provided `api.yml` and tf configs
* RECAPTCHAv2 validation is required
* CORS validation is supported

Once deployed, you can send a message via HTTP POST to invoke the function.

## Configuration

### Environment Variables

```
CONTACTAGON_CORS_ORIGINS             # comma-separated list of CORS origins to allow
CONTACTAGON_DEBUG                    # true|false sets debug log level etc
CONTACTAGON_FIRESTORE_COLLECTION     # name of the firestore collection
CONTACTAGON_FROM_ADDRESS             # sender address (must be sendgrid verified)
CONTACTAGON_PRETTY_LOGS              # true|false enable for development
CONTACTAGON_RECAPTCHA_SECRET_KEY     # needed to verify recaptcha
CONTACTAGON_RECAPTCHA_VERIFY_URL     # url for recaptcha verify
CONTACTAGON_SANITIZE                 # true|false sanitize message body as if HTML
CONTACTAGON_SENDGRID_API_KEY         # needed to send mail
CONTACTAGON_TO_ADDRESS               # address to forward messages to
CONTACTAGON_TO_NAME                  # name to forward messages to
GCP_PROJECT                          # GCP project id

```

### Secrets

The following secrets are read first from the environment variable, if set:

```
CONTACTAGON_RECAPTCHA_SECRET_KEY
CONTACTAGON_SENDGRID_API_KEY
```

If the environment variable is not set, contactagon will attempt to look up a secret of the same key from Google Secret Manager. (Note that this requires the function to execute as a service account that can read these secrets.)

## Deployment

See `README.md` in the `tf` folder.