package contactagon

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"net/http/httptest"
	"testing"
)

func TestContains(t *testing.T) {
	strings := []string{"asdf", "1234", "zxcv", "mnbv"}
	assert.True(t, contains(strings, "zxcv"))
}

func TestContainsNot(t *testing.T) {
	strings := []string{"asdf", "1234", "zxcv", "mnbv"}
	assert.False(t, contains(strings, "poiu"))
}

type SuiteRemoteIP struct {
	suite.Suite
}

func (suite *SuiteRemoteIP) TestGetRemoteIpXffComma() {
	request := httptest.NewRequest("GET","/", nil)
	request.Header.Add("X-Forwarded-For", "10.11.12.15, 10.11.12.20")
	remoteIp := getRemoteIP(request)
	assert.Equal(suite.T(), "10.11.12.15", remoteIp)
}

func (suite *SuiteRemoteIP) TestGetRemoteIpXffIp() {
	request := httptest.NewRequest("GET","/", nil)
	request.Header.Add("X-Forwarded-For", "10.11.12.13")
	remoteIp := getRemoteIP(request)
	assert.Equal(suite.T(), "10.11.12.13", remoteIp)
}

func (suite *SuiteRemoteIP) TestGetRemoteIpXffNone() {
	request := httptest.NewRequest("GET", "/", nil)
	remoteIp := getRemoteIP(request)
	assert.Equal(suite.T(), "192.0.2.1", remoteIp)
}

func (suite *SuiteRemoteIP) TestGetRemoteIpXffSpace() {
	request := httptest.NewRequest("GET","/", nil)
	request.Header.Add("X-Forwarded-For", "10.11.12.25 10.11.12.20")
	remoteIp := getRemoteIP(request)
	assert.Equal(suite.T(), "10.11.12.25", remoteIp)
}

func TestSuiteRemoteIP(t *testing.T) {
	suite.Run(t, new(SuiteRemoteIP))
}

func TestRandomString(t *testing.T) {
	randstr := randomString(41)
	assert.Regexp(t, "[abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ]{41}", randstr)
}
