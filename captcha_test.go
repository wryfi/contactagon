package contactagon

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"testing"
	"time"
)

type MockCaptchaClient struct {
	mock.Mock
}

func (ccmock *MockCaptchaClient) Verify(captchaData, remoteip string) (CaptchaVerifyResponse, error) {
	if captchaData == "pass" && remoteip != "0.0.0.0" {
		response := CaptchaVerifyResponse{
			Success:     true,
			Score:       10,
			Action:      "huzzah",
			ChallengeTS: time.Time{},
			Hostname:    "foo.bar.com",
			ErrorCodes:  nil,
		}
		return response, nil
	} else if captchaData == "pass" && remoteip == "0.0.0.0" {
		return CaptchaVerifyResponse{
			Success:     false,
			Score:       0,
			Action:      "",
			ChallengeTS: time.Time{},
			Hostname:    "foo.bar.com",
			ErrorCodes:  nil,
		}, nil
	}
	return CaptchaVerifyResponse{}, errors.New("mock error")
}

type SuiteCaptcha struct {
	suite.Suite
	captchaService *CaptchaService
	captchaFail CaptchaVerifyResponse
	captchaPass CaptchaVerifyResponse
}

func (suite *SuiteCaptcha) SetupSuite() {
	suite.captchaFail = CaptchaVerifyResponse{
		Success:     false,
		Score:       0,
		Action:      "",
		ChallengeTS: time.Time{},
		Hostname:    "foo.bar.com",
		ErrorCodes:  nil,
	}
	suite.captchaPass = CaptchaVerifyResponse{
		Success:     true,
		Score:       10,
		Action:      "huzzah",
		ChallengeTS: time.Time{},
		Hostname:    "foo.bar.com",
		ErrorCodes:  nil,
	}
	suite.captchaService = captchaService
	captchaClientMock := new(MockCaptchaClient)
	mockCaptchaService := CaptchaService{client: captchaClientMock}
	captchaService = &mockCaptchaService
}

func (suite *SuiteCaptcha) TearDownSuite() {
	captchaService = suite.captchaService
}

func (suite *SuiteCaptcha) TestCaptchaVerify() {
	response, err := CaptchaVerify("pass", "1.2.3.4")
	assert.Equal(suite.T(), suite.captchaPass, response)
	assert.NoError(suite.T(), err)
}

func (suite *SuiteCaptcha) TestCaptchaVerifyError() {
	response, err := CaptchaVerify("error", "0.0.0.0")
	assert.Equal(suite.T(), CaptchaVerifyResponse{}, response)
	assert.Error(suite.T(), err, "mock error")
}

func (suite *SuiteCaptcha) TestCaptchaVerifyFail() {
	response, err := CaptchaVerify("pass", "0.0.0.0")
	assert.Equal(suite.T(), suite.captchaFail, response)
	assert.NoError(suite.T(), err)
}

func TestSuiteCaptcha(t *testing.T) {
	suite.Run(t, new(SuiteCaptcha))
}