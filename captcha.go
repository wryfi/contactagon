package contactagon

import (
	"encoding/json"
	"net/http"
	"os"
	"time"
)

type CaptchaVerifyResponse struct {
	Success     bool      `json:"success"`
	Score       float64   `json:"score"`
	Action      string    `json:"action"`
	ChallengeTS time.Time `json:"challenge_ts"`
	Hostname    string    `json:"hostname"`
	ErrorCodes  []string  `json:"error-codes"`
}

type CaptchaService struct {
	client CaptchaClient
}

type CaptchaClient interface {
	Verify(string, string) (CaptchaVerifyResponse, error)
}

var captchaService *CaptchaService

func init() {
	captchaService = NewCaptchaService(new(RecaptchaClient))
}

func NewCaptchaService(client CaptchaClient) *CaptchaService {
	return &CaptchaService{client: client}
}

type RecaptchaClient struct {}

func (recaptcha *RecaptchaClient) Verify(captchaData, remoteip string) (CaptchaVerifyResponse, error) {
	request, err := http.NewRequest(http.MethodPost, os.Getenv("CONTACTAGON_RECAPTCHA_VERIFY_URL"), nil)
	if err != nil {
		return CaptchaVerifyResponse{}, err
	}
	params := request.URL.Query()
	secret, err := GetEnvOrSecret("CONTACTAGON_RECAPTCHA_SECRET_KEY")
	if err != nil {
		return CaptchaVerifyResponse{}, err
	}
	params.Add("secret", secret)
	params.Add("response", captchaData)
	params.Add("remoteip", remoteip)
	request.URL.RawQuery = params.Encode()
	response, err := http.DefaultClient.Do(request)
	if err != nil {
		return CaptchaVerifyResponse{}, err
	}
	defer response.Body.Close()
	var verification CaptchaVerifyResponse
	if err = json.NewDecoder(response.Body).Decode(&verification); err != nil {
		return CaptchaVerifyResponse{}, err
	}
	return verification, nil
}

func CaptchaVerify(captchaData string, remoteip string) (CaptchaVerifyResponse, error) {
	response, err := captchaService.client.Verify(captchaData, remoteip)
	if err != nil {
		logger.Error().Msgf("captcha verification fail: %s", err)
	}
	return response, err
}
