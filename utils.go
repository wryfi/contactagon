package contactagon

import (
	"math/rand"
	"net/http"
	"regexp"
)

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func contains(strslice []string, str string) bool {
	for _, value := range strslice {
		if value == str {
			return true
		}
	}
	return false
}

func getRemoteIP(request *http.Request) string {
	xff := request.Header.Get("x-forwarded-for")
	if len(xff) > 0 {
		commaRegex := regexp.MustCompile(`.*,\s*.*`)
		spacesRegex := regexp.MustCompile(`.*\s+.*`)
		commaSplit := regexp.MustCompile(`,\s*`)
		spaceSplit := regexp.MustCompile(`\s+`)
		if match := commaRegex.MatchString(xff); match == true {
			return commaSplit.Split(xff, -1)[0]
		} else if match := spacesRegex.MatchString(xff); match == true {
			return spaceSplit.Split(xff, -1)[0]
		} else {
			return xff
		}
	} else {
		ipRegex := regexp.MustCompile(`(.*):\d{2,5}$`)
		if match := ipRegex.MatchString(request.RemoteAddr); match == true {
			submatch := ipRegex.FindStringSubmatch(request.RemoteAddr)
			return submatch[1]
		}
	}
	return ""
}

func randomString(strLen int) string {
	outBytes := make([]byte, strLen)
	for i := range outBytes {
		outBytes[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(outBytes)
}
