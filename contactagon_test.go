package contactagon

import (
	"bytes"
	"encoding/json"
	"github.com/rs/zerolog/log"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"net/http/httptest"
	"os"
	"testing"
)

type SuiteMessagePost struct {
	suite.Suite
	Message Message
}

func (suite *SuiteMessagePost) SetupTest() {
	suite.Message = Message{
		Name:    "John Doe",
		Email:   "sender@testing.local",
		Phone:   "12345678901",
		ReplyBy: "email",
		Subject: "hello my friend",
		Message: "I have a proposal for you!",
		Captcha: "pass",
		Time:    1633275714,
		IP:      "1.2.3.4",
	}
}

func (suite *SuiteMessagePost) SetupSuite() {
	secretService = NewSecretService(new(MockSecretProvider))
	storageService = NewStorageService(new(MockStorageProvider))
	captchaService = NewCaptchaService(new(MockCaptchaClient))
	mailService = NewMailService(new(MockMailProvider))
}

func (suite *SuiteMessagePost) TearDownSuite() {
	secretService = NewSecretService(new(GoogleSecretProvider))
	storageService = NewStorageService(new(FirestoreClient))
	captchaService = NewCaptchaService(new(RecaptchaClient))
	mailService = NewMailService(new(SendGridClient))
}

func (suite *SuiteMessagePost) TestMessagePost() {
	os.Setenv("CONTACTAGON_CORS_ORIGINS", "https://test.local")
	defer os.Unsetenv("CONTACTAGON_CORS_ORIGINS")
	messageJson, err := json.Marshal(suite.Message)
	if err != nil {
		log.Error().Msgf("%s", err)
	}
	request := httptest.NewRequest("POST", "/", bytes.NewReader(messageJson))
	request.Header.Set("origin", "https://test.local")
	writer := httptest.NewRecorder()
	MessagePost(writer, request)
	assert.Equal(suite.T(), 202, writer.Code)
}

func TestSuiteMessagePost(t *testing.T) {
	suite.Run(t, new(SuiteMessagePost))
}