package contactagon

import (
	"errors"
	"github.com/rs/zerolog/log"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"testing"
)

type MockMailProvider struct {
	mock.Mock
}

func (mmock *MockMailProvider) SendMail(message *mail.SGMailV3) error {
	if message.From.Address == "no-reply@testing.local" {
		return errors.New("mock error sending mail")
	}
	return nil
}

type SuiteMail struct {
	suite.Suite
	OriginalService *MailService
	Message         *mail.SGMailV3
}

func (suite *SuiteMail) SetupSuite() {
	suite.Message = mail.NewSingleEmailPlainText(
		mail.NewEmail("test sender", "sender@testing.local"),
		"hello, world",
		mail.NewEmail("test recipient", "receiver@testing.local"),
		"is anyone listening?",
	)
	suite.OriginalService = mailService
	mockMailService := NewMailService(new(MockMailProvider))
	mailService = mockMailService
}

func (suite *SuiteMail) TearDownSuite() {
	mailService = suite.OriginalService
}

func (suite *SuiteMail) TestSend() {
	err := SendMail(suite.Message)
	if err != nil {
		log.Error().Msg(err.Error())
	}
	assert.NoError(suite.T(), err)
}

func (suite *SuiteMail) TestSendFail() {
	suite.Message.From.Address = "no-reply@testing.local"
	err := SendMail(suite.Message)
	assert.Error(suite.T(), err, "mock error sending mail")
}

func TestSuiteMail(t *testing.T) {
	suite.Run(t, new(SuiteMail))
}
