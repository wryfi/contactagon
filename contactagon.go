package contactagon

import (
	"encoding/json"
	"net/http"
)

// MessagePost is an API / Cloud Functions entry point, which receives POST
// requests (e.g. from GCP) to the application. It is the only entry point.
func MessagePost(writer http.ResponseWriter, request *http.Request) {
	manageCors(writer, request)
	if request.Method == "OPTIONS" || request.Method == "HEAD" {
		writer.WriteHeader(http.StatusNoContent)
		return
	} else if request.Method == "POST" {
		var message Message
		if err := json.NewDecoder(request.Body).Decode(&message); err != nil {
			http.Error(writer, "error decoding JSON", 400)
			logger.Error().Msgf("error decoding JSON from %s", request.RemoteAddr)
			return
		}
		message.HandlePost(writer, request)
		return
	} else {
		http.Error(writer, "http/405 method not allowed", 405)
		return
	}
}
