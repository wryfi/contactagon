package contactagon

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"os"
	"testing"
)

type MockSecretProvider struct {
	mock.Mock
}

func (ssmock *MockSecretProvider) GetSecret(string) (string, error) {
	return "psst quiet", nil
}

type SuiteSecrets struct {
	suite.Suite
	secretService  *SecretService
}

func (suite *SuiteSecrets) SetupSuite() {
	suite.secretService = secretService
	secretProviderMock := new(MockSecretProvider)
	mockSecretService := SecretService{client: secretProviderMock}
	secretService = &mockSecretService
}

func (suite *SuiteSecrets) TearDownSuite() {
	secretService = suite.secretService
}

func (suite *SuiteSecrets) TestGetEnvOrSecret() {
	secret, err := GetEnvOrSecret("foo")
	assert.Equal(suite.T(), "psst quiet", secret)
	assert.NoError(suite.T(), err)
}

func (suite *SuiteSecrets) TestGetEnvOrSecretWithEnv() {
	os.Setenv("CONTACTAGON_TEST_SECRET", "asdf1234")
	defer os.Unsetenv("CONTACTAGON_TEST_SECRET")
	secret, err := GetEnvOrSecret("CONTACTAGON_TEST_SECRET")
	assert.Equal(suite.T(), "asdf1234", secret)
	assert.NoError(suite.T(), err)
}

func (suite *SuiteSecrets) TestGetSecret() {
	secret, err := GetSecret("foo")
	assert.Equal(suite.T(), "psst quiet", secret)
	assert.NoError(suite.T(), err)
}

func TestSuiteSecrets(t *testing.T) {
	suite.Run(t, new(SuiteSecrets))
}
