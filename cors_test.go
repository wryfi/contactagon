package contactagon

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"net/http/httptest"
	"os"
	"testing"
)

type SuiteCors struct {
	suite.Suite
}

func (suite *SuiteCors) TestCorsAllowedOriginSingle() {
	os.Setenv("CONTACTAGON_CORS_ORIGINS", "https://cv.wryfi.net")
	defer os.Unsetenv("CONTACTAGON_CORS_ORIGINS")
	request := httptest.NewRequest("GET", "/", nil)
	request.Header.Set("origin", "https://cv.wryfi.net")
	writer := httptest.NewRecorder()
	manageCors(writer, request)
	assert.Equal(suite.T(), "https://cv.wryfi.net", writer.Header().Get("Access-Control-Allow-Origin"))
}

func (suite *SuiteCors) TestCorsAllowedOriginMultiple() {
	os.Setenv("CONTACTAGON_CORS_ORIGINS", "https://cv.wryfi.net,https://contacts.wryfi.net")
	defer os.Unsetenv("CONTACTAGON_CORS_ORIGINS")
	request := httptest.NewRequest("GET", "/", nil)
	request.Header.Set("origin", "https://contacts.wryfi.net")
	writer := httptest.NewRecorder()
	manageCors(writer, request)
	assert.Equal(suite.T(), "https://contacts.wryfi.net", writer.Header().Get("Access-Control-Allow-Origin"))
}

func (suite *SuiteCors) TestCorsAllowedOriginNone() {
	os.Unsetenv("CONTACTAGON_CORS_ORIGINS")
	request := httptest.NewRequest("GET", "/", nil)
	request.Header.Set("origin", "https://contacts.wryfi.net")
	writer := httptest.NewRecorder()
	manageCors(writer, request)
	assert.Equal(suite.T(), "", writer.Header().Get("Access-Control-Allow-Origin"))
}

func (suite *SuiteCors) TestCorsAllowedOriginNoMatch() {
	os.Setenv("CONTACTAGON_CORS_ORIGINS", "https://cv.wryfi.net,https://contacts.wryfi.net")
	defer os.Unsetenv("CONTACTAGON_CORS_ORIGINS")
	request := httptest.NewRequest("GET", "/", nil)
	request.Header.Set("origin", "https://wry.fi")
	writer := httptest.NewRecorder()
	manageCors(writer, request)
	assert.Equal(suite.T(), "", writer.Header().Get("Access-Control-Allow-Origin"))
}

func (suite *SuiteCors) TestCorsAllowedMethods() {
	request := httptest.NewRequest("GET", "/", nil)
	writer := httptest.NewRecorder()
	manageCors(writer, request)
	assert.Equal(suite.T(), "HEAD, OPTIONS, POST", writer.Header().Get("Access-Control-Allow-Methods"))
}

func (suite *SuiteCors) TestCorsAllowedHeaders() {
	request := httptest.NewRequest("GET", "/", nil)
	writer := httptest.NewRecorder()
	manageCors(writer, request)
	assert.Equal(
		suite.T(),
		"Accept, Accept-Encoding, Content-Length, Content-Type, Date, Server, Transfer-Encoding",
		writer.Header().Get("Access-Control-Allow-Headers"),
	)
}

func TestSuiteCors(t *testing.T) {
	suite.Run(t, new(SuiteCors))
}
