package contactagon

import (
	"cloud.google.com/go/firestore"
	secretmanager "cloud.google.com/go/secretmanager/apiv1"
	"context"
	"github.com/rs/zerolog/log"
	"os"
)

var fire *firestore.Client
var fireCtx context.Context

func init() {
	var err error
	fireCtx = context.Background()
	fire, err = firestore.NewClient(fireCtx, os.Getenv("GCP_PROJECT"))
	if err != nil {
		log.Fatal().Msgf("error initializing firestore client: %s", err)
	}

	secretCtx = context.Background()
	secretManager, err = secretmanager.NewClient(secretCtx)
	if err != nil {
		log.Fatal().Msgf("error initializing cloud secrets client: %s", err)
	}

	secretProvider := new(GoogleSecretProvider)
	secretService = NewSecretService(secretProvider)
}
