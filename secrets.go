package contactagon

import (
	secretmanager "cloud.google.com/go/secretmanager/apiv1"
	"context"
	"fmt"
	secretmanagerpb "google.golang.org/genproto/googleapis/cloud/secretmanager/v1"
	"os"
)

var secretManager *secretmanager.Client
var secretCtx context.Context
var secretService *SecretService

func init() {
	var err error
	secretCtx = context.Background()
	secretManager, err = secretmanager.NewClient(secretCtx)
	if err != nil {
		logger.Fatal().Msgf("error initializing cloud secrets client: %s", err)
	}
	secretProvider := new(GoogleSecretProvider)
	secretService = NewSecretService(secretProvider)
}

type SecretClient interface {
	GetSecret(string) (string, error)
}

type SecretService struct {
	client SecretClient
}

func NewSecretService(client SecretClient) *SecretService {
	return &SecretService{client: client}
}

type GoogleSecretProvider struct {}

func (gcp *GoogleSecretProvider) GetSecret(secretId string) (string, error) {
	req := &secretmanagerpb.AccessSecretVersionRequest{Name: secretId}
	result, err := secretManager.AccessSecretVersion(secretCtx, req)
	if err != nil {
		logger.Error().Msgf("error requesting secret: %s", err)
		return "", err
	}
	return string(result.Payload.Data), nil
}

func GetSecret(secretId string) (string, error) {
	deciphered, err := secretService.client.GetSecret(secretId)
	if err != nil {
		return "", err
	}
	return deciphered, nil
}

func GetEnvOrSecret(keyname string) (string, error) {
	projectId := os.Getenv("GCP_PROJECT")
	fromEnv := os.Getenv(keyname)
	if len(fromEnv) > 0 {
		logger.Debug().Msgf("found %s in environment", keyname)
		return fromEnv, nil
	} else {
		secretName := fmt.Sprintf("projects/%s/secrets/%s/versions/latest", projectId, keyname)
		fromSecret, err := GetSecret(secretName)
		if err == nil && len(fromSecret) > 0 {
			return fromSecret, nil
		} else {
			logger.Error().Msgf("error getting secret: %s", err)
			return "", err
		}
	}
}

