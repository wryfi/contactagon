package contactagon

import (
	"cloud.google.com/go/firestore"
	"google.golang.org/api/iterator"
	"os"
	"time"
)

var storageService *StorageService

func init() {
	collection := os.Getenv("CONTACTAGON_FIRESTORE_COLLECTION")
	storageProvider := FirestoreClient{collection: fire.Collection(collection)}
	storageService = NewStorageService(storageProvider)
}

type StorageClient interface {
	GetMessagesSince(int64, string) []Message
	Save(string, Message) error
}

type StorageService struct {
	client StorageClient
}

func NewStorageService(client StorageClient) *StorageService {
	return &StorageService{client: client}
}

type FirestoreClient struct {
	collection *firestore.CollectionRef
}

func (fsclient FirestoreClient) GetMessagesSince(time int64, ip string) []Message {
	var messages []Message
	query := fsclient.collection.Where("ip", "==", ip).Where("time", ">", time).OrderBy("time", firestore.Desc)
	iter := query.Documents(fireCtx)
	defer iter.Stop()
	for {
		doc, err := iter.Next()
		if err == iterator.Done {
			break
		}
		var msg Message
		err = doc.DataTo(&msg)
		if err == nil {
			messages = append(messages, msg)
		} else {
			logger.Error().Msgf("error converting Message to struct: %s", err)
		}
	}
	return messages
}

func (fsclient FirestoreClient) Save(remoteAddr string, message Message) error {
	collection := fire.Collection(os.Getenv("CONTACTAGON_FIRESTORE_COLLECTION"))
	message.Time = time.Now().Unix()
	message.IP = remoteAddr
	_, _, err := collection.Add(fireCtx, message)
	if err != nil {
		logger.Error().Msgf("error saving Message to firestore: %s", err)
		return err
	} else {
		logger.Info().Msgf("saved Message from %s to firestore", remoteAddr)
		return nil
	}
}
